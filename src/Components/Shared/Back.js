import styled from 'styled-components'

const Back = styled.img`
  filter: invert(99%) sepia(46%) saturate(742%) hue-rotate(301deg)
    brightness(115%) contrast(91%);
  width: 2rem;
  margin-top: 1rem;
`
export default Back
