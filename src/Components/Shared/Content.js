import styled from 'styled-components'
import { animated } from 'react-spring'

const Content = styled(animated.div)`
  > * {
    margin-top: 0.75rem;
  }
`
export default Content
