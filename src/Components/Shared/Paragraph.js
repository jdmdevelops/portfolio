import styled from 'styled-components'

const Paragraph = styled.div`
  line-height: 1.3;
  font-family: 'ABeeZee', sans-serif;
`

export default Paragraph
