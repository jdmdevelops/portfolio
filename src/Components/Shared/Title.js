import styled from 'styled-components'

const Title = styled.div`
  color: #18ccf1;
  font-size: 4.5rem;
  font-weight: bold;
`
export default Title
